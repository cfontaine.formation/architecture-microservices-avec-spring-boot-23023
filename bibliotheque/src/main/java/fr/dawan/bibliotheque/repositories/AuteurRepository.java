package fr.dawan.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.bibliotheque.entities.Auteur;

//@Repository
public interface AuteurRepository extends JpaRepository<Auteur, Long> {

    // Tous les auteurs vivant de manière paginé
    List<Auteur> findByDecesIsNull(Pageable page);
    
    // tous les auteurs en faisant une recherche par le nom
    List<Auteur> findByNomIgnoreCase(String nom);
    
    // Les auteurs qui ont écrit une livre, on passe en paramètre le nom du livre
    @Query("SELECT a FROM Auteur a JOIN a.livres l WHERE l.titre=:titre")
    List<Auteur> findByNomLivre(@Param("titre") String titre);
    
    // le classement des auteurs classé par nombre de livre écrit décroissant
    @Query("SELECT a FROM Auteur a JOIN a.livres l GROUP BY a ORDER BY COUNT(l) DESC")
    List<Auteur> findByTopAuteurNombreLivre();
}

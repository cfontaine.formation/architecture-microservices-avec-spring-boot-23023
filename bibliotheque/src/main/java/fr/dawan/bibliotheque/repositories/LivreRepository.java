package fr.dawan.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.dawan.bibliotheque.entities.Livre;

public interface LivreRepository extends JpaRepository<Livre, Long> {

    // pour faire un recherche avec l'opérateur Like sur les titres
    List<Livre> findByTitreLike(String motif);
    
    // qui retourne tous les livres sorties pour un intervalle d'année min et max passé en paramètres
    List<Livre> findByAnneeSortieBetween(int anneeMin, int anneeMax);
    
    // tous les livres d'une categorie trié par année de sortie décroissant, on passe en paramètre le nom de la categorie
    List<Livre> findByCategorieNomOrderByAnneeSortieDesc(String nomCategeorie);
    
    // qui retourne le nombre de livre sortie l'année passé en paramètre
    int countByAnneeSortie(int anneeSortie);
    
    // qui retourne tous les livres qui ont plusieurs auteurs  (requête en JPQL)
    @Query("SELECT l FROM Livre l JOIN l.auteurs a GROUP BY l HAVING COUNT(a)>1")
    List<Livre> findByMultiAuteur();
    
   // qui retourne l'année où est sortie le plus de livre  (requête en JPQL)
    @Query(nativeQuery = true,value="SELECT livres.annee_sortie FROM livres GROUP BY livres.annee_sortie ORDER BY count(livres.id) DESC LIMIT 1")
    int findByAnneeSortieMaxLivre();
    
}

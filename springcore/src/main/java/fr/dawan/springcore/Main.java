package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.DtoMapper;
import fr.dawan.springcore.repositories.ArticleRepository;
import fr.dawan.springcore.services.ArticleService;

public class Main {
    public static void main( String[] args )
    {
       ApplicationContext ctx=new AnnotationConfigApplicationContext(AppConf.class) ;
       System.out.println("-----------------------------------");
       
       DtoMapper dto1=ctx.getBean("dto1",DtoMapper.class);
       System.out.println(dto1);
       ArticleRepository repo1=ctx.getBean("repository1",ArticleRepository.class);
       System.out.println(repo1);
       ArticleService serv1=ctx.getBean("service1",ArticleService.class);
       System.out.println(serv1);
       ArticleService serv2=ctx.getBean("service2",ArticleService.class);
       System.out.println(serv2);
       
       // Singleton (par défaut)
       ArticleService serv11=ctx.getBean("service1",ArticleService.class);
       System.out.println(serv11);
       ArticleService serv12=ctx.getBean("service1",ArticleService.class);
       System.out.println(serv12);
       
       // Prototype
       ArticleRepository rep11=ctx.getBean("repository1",ArticleRepository.class);
       System.out.println(rep11);
       ArticleRepository rep12=ctx.getBean("repository1",ArticleRepository.class);
       System.out.println(rep12);
       
       
       ((AbstractApplicationContext)ctx).close();
    }
}

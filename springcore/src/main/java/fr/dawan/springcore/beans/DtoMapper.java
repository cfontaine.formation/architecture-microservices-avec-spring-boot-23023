package fr.dawan.springcore.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class DtoMapper implements Serializable {

    private static final long serialVersionUID = 1L;

    private String source;

    private String target;

    public DtoMapper() {
        System.out.println("constructeur DtoMapper");
    }

    public DtoMapper(String source, String target) {
        this.source = source;
        this.target = target;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public String toString() {
        return "DtoMapper [source=" + source + ", target=" + target + "]";
    }
    
    @PostConstruct
    public void init() {
        System.out.println("Méthode init");
    }
    
    @PreDestroy
    public void destroy() {
        System.out.println("Méthode destroy");
    }

}

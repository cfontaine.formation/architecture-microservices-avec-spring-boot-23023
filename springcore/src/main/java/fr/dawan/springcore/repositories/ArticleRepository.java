package fr.dawan.springcore.repositories;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@Repository("repository1")
@Scope("prototype")
public class ArticleRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    private String data;

    public ArticleRepository() {
        System.out.println("Constructeur par défaut: Article");
    }

    public ArticleRepository(String data) {
        System.out.println("Constructeur un paramètre: Article");
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        System.out.println("Setter: Article");
        this.data = data;
    }

    @Override
    public String toString() {
        return "ArticleRepository [data=" + data + ", toString()=" + super.toString() + "]";
    }

}

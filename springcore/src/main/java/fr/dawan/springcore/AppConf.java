package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import fr.dawan.springcore.beans.DtoMapper;
import fr.dawan.springcore.repositories.ArticleRepository;
import fr.dawan.springcore.services.ArticleService;

@Configuration
@ComponentScan(basePackages = "fr.dawan.springcore")
public class AppConf {

    @Bean//(initMethod = "init",destroyMethod = "destroy")
    public DtoMapper dto1() {
        return new DtoMapper();
    }

    @Bean
    public ArticleRepository repository2() {
        return new ArticleRepository();
    }
    
    @Bean
    @Lazy
    public ArticleService service1(ArticleRepository repository1)
    {
        return new ArticleService(repository1);
    }
    
//  @Bean 
//  public ArticleService service1()
//  {
//      return new ArticleService(repository2());
//  }
    
}

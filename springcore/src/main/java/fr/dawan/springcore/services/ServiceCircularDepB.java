package fr.dawan.springcore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceCircularDepB {

    private ServiceCircularDepA serviceA;

    // Un seul constructeur => @Autowired implicite
    public ServiceCircularDepB(ServiceCircularDepA serviceA) {
        System.out.println("Constructeur CircularDepB");
        this.serviceA = serviceA;
    }

    public ServiceCircularDepA getServiceA() {
        return serviceA;
    }

    @Autowired
    public void setServiceA(ServiceCircularDepA serviceA) {
        System.out.println("Setter Service B :   " + serviceA);
        this.serviceA = serviceA;
    }

    @Override
    public String toString() {
        return "ServiceB [serviceA=" + serviceA + ", toString()=" + super.toString() + "]";
    }

}

package fr.dawan.springcore.services;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import fr.dawan.springcore.repositories.ArticleRepository;

@Service("service2")
public class ArticleService implements Serializable {

    private static final long serialVersionUID = 1L;

  //  @Autowired//(required = false)
    //@Qualifier("repository2")
    private ArticleRepository repository;

    public ArticleService() {
        System.out.println("Constructeur par défaut Service");
    }

   // @Autowired
    public ArticleService(/*@Qualifier("repository2")*/ArticleRepository repository) {
        this.repository = repository;
        System.out.println("Constructeur un paramètre Service");
    }

    public ArticleRepository getRepository() {
        return repository;
    }

   // @Autowired
    public void setRepository(/*@Qualifier("repository2")*/ArticleRepository repository) {
        System.out.println("Setter Service");
        this.repository = repository;
    }

    @Override
    public String toString() {
        return "ArticleService [repository=" + repository + ", toString()=" + super.toString() + "]";
    }

}

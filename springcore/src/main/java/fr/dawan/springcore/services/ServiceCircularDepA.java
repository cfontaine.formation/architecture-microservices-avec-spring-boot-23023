package fr.dawan.springcore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceCircularDepA {

    private ServiceCircularDepB serviceB;

    public ServiceCircularDepA() {
        System.out.println("Constructeur par défaut ServiceA");
    }

   // @Autowired
    public ServiceCircularDepA(ServiceCircularDepB serviceB) {
        System.out.println("Constructeur ServiceA");
        this.serviceB = serviceB;
    }

    public ServiceCircularDepB getServiceB() {
        return serviceB;
    }

    @Autowired
    public void setServiceB(ServiceCircularDepB serviceB) {
        System.out.println("Setter ServiceA :   " + serviceB);
        this.serviceB = serviceB;
    }

    @Override
    public String toString() {
        return "ServiceA [serviceB=" + serviceB + ", toString()=" + super.toString() + "]";
    }

}

package fr.dawan.clientapi;

import java.time.LocalDate;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class RestConsumer {
    
    private static final String url="http://localhost:8080/api/articles";
    
    public void getArticleJson() {
        RestTemplate restTemplate =new RestTemplate();
        
        ResponseEntity<String> response= restTemplate.getForEntity(url, String.class);
        System.out.println(response.getStatusCodeValue());
        System.out.println(response.getBody());
    }
    
    public void getArticles() {
        RestTemplate restTemplate =new RestTemplate();

        List<?> articles=restTemplate.getForEntity(url, List.class).getBody();
        articles.forEach(System.out::println);
    }
    
    
    
    public void createArticle() {
        RestTemplate restTemplate =new RestTemplate();
        ArticleDto a=new ArticleDto();
        a.setDateProduction(LocalDate.now());
        a.setDescription("bouteille d'eau 1l");
        a.setPrix(0.9);
        
        HttpEntity<ArticleDto> request=new HttpEntity<>(a);
        
        ResponseEntity<String>rep=restTemplate.exchange(url, HttpMethod.POST,request,String.class);
        System.out.println(rep.getStatusCodeValue());
    }
    
    public static void main(String[] args) {
            RestConsumer rc=new RestConsumer();
            //rc.getArticleJson();
            //rc.getArticles();
            rc.createArticle();
        }

}

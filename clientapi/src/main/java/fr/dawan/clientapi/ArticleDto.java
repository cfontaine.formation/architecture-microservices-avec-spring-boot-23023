package fr.dawan.clientapi;

import java.time.LocalDate;

public class ArticleDto {
    

    private long id;
    

    private double prix;

    private String description;

    private LocalDate dateProduction;

    private byte[] image;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDateProduction() {
        return dateProduction;
    }

    public void setDateProduction(LocalDate dateProduction) {
        this.dateProduction = dateProduction;
    }
    
    

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "ArticleDto [id=" + id + ", prix=" + prix + ", description=" + description + ", dateProduction="
                + dateProduction + "]";
    }

}

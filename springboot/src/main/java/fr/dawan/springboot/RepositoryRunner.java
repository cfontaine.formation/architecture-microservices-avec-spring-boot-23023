package fr.dawan.springboot;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import fr.dawan.springboot.entities.Article;
import fr.dawan.springboot.entities.Marque;
import fr.dawan.springboot.repositories.ArticleRepository;
import fr.dawan.springboot.repositories.CustomRepository;
import fr.dawan.springboot.repositories.MarqueRepository;

//@Component
//@Order(1)
public class RepositoryRunner implements ApplicationRunner {
    
    @Autowired
    private ArticleRepository articleRepository;
    
    @Autowired
    private MarqueRepository marqueRepository;
    
    @Autowired
    private CustomRepository employeRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
       System.out.println("Repository Runner");
       System.out.println("------------------");
       List<Article> lst=articleRepository.findAll();
       for(Article a : lst) {
           System.out.println(a);
       }
       
       articleRepository.findByDescriptionLike("M___%").forEach(System.out::println);
    
       articleRepository.findByPrixBetween(20.0,70.0).forEach(System.out::println);
       
       System.out.println(articleRepository.countByPrixLessThan(30.0));
       
       System.out.println(articleRepository.existsByDateProductionAfter(LocalDate.of(2020, Month.JULY, 14)));
       
       Marque m1=new Marque("Marque F");
       Article a1=new Article("Stylo bille noir",1.4,LocalDate.of(2018, 1, 5));
       a1.setMarque(m1);
       m1.getArticles().add(a1);
       articleRepository.saveAndFlush(a1);
       
       articleRepository.findByPrixLessThanJPQL(150.0).forEach(System.out::println);
       articleRepository.findByPrixLessThanSQL(50.0).forEach(System.out::println);
       
       System.out.println(marqueRepository.findJointure(4));
       
       articleRepository.findByMarqueNomIgnoreCase("Marque A").forEach(System.out::println);
   
       // Pagination
       Page<Article>page=articleRepository.findByPrixLessThan(1000.0, PageRequest.of(1, 3));
       System.out.println(page.getTotalPages() + " / " + page.getTotalElements());
       page.forEach(System.out::println);
       
       articleRepository.findByPrixLessThan(45.0, Pageable.unpaged()).forEach(System.out::println);
       
       articleRepository.findByPrixLessThan(1000.0, PageRequest.of( 1, 5,Sort.by("description").and(Sort.by("prix").descending()))).forEach(System.out::println);
    
       System.out.println(articleRepository.findTopByOrderByPrixDesc());
       
       articleRepository.findTop5ByOrderByPrix().forEach(System.out::println);
       
       employeRepository.findBy("John", null).forEach(System.out::println);
       
       employeRepository.findBy(null, "Doe").forEach(System.out::println);
       
      employeRepository.findBy("John", "Doe").forEach(System.out::println);

//      System.out.println(articleRepository.countINFMontant(20.0));
//      //System.out.println(articleRepository.countINFMontantSQL(20.0));
//      System.out.println(articleRepository.get_count_by_prix2(20.0));
   
      Optional<Article> optA=articleRepository.findById(12L);
      Article am=optA.get();
      am.setPrix(4.0);
      articleRepository.save(am);
      System.out.println("-1---------------------");
      articleRepository.findByPrixLessThan(45.0).forEach(System.out::println);
      System.out.println("-2---------------------");
      articleRepository.findByPrixLessThan(45.0).forEach(System.out::println);;
    }

}

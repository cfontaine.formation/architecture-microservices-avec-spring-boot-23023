package fr.dawan.springboot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springboot.ConfigureProperties;

@RestController
public class HelloWorldController {
    
    @Value("${hello.message}")
    private String msg;
    
    @Autowired
    private ConfigureProperties conf;
    
    @RequestMapping("/hello")
    public String hello() {
        return msg + " " +conf.getHost();
    }

}

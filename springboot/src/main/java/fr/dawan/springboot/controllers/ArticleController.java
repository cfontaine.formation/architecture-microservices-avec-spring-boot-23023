package fr.dawan.springboot.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.dawan.springboot.dto.ArticleDto;
import fr.dawan.springboot.services.ArticleService;
import fr.dawan.springboot.util.ApiError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/articles")
@Tag(name="Articles", description="L'api des __articles__")
public class ArticleController {

    @Autowired
    private ArticleService service;
    
    @GetMapping(value = "",produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}) //"application/json"
    public List<ArticleDto> getAllArticle() {
        return service.getAllArticle(Pageable.unpaged());
    }
    
    @GetMapping(value="",params= {"page","size"},produces=MediaType.APPLICATION_JSON_VALUE)
    public List<ArticleDto> getAllArticle(Pageable page){
        return service.getAllArticle(page);
    }
    
    @Operation(summary = "Trouver des articles à partir de leurs Id",description="retourne un article",tags= {"Articles"})
    @ApiResponses(value= {@ApiResponse(responseCode = "200", description="opération réussit",
                content=@Content(schema=@Schema(implementation=ArticleDto.class))),
            @ApiResponse(responseCode = "404", description="Article non trouvé",
            content=@Content(schema=@Schema(implementation=ApiError.class)))
    })
    @GetMapping(value="/{id:[0-9]+}",produces=MediaType.APPLICATION_JSON_VALUE)
    public ArticleDto getArticleById(
            @Parameter(description = "L'id de 'article que l'on veut obtenir",required = true,allowEmptyValue = false)
            @PathVariable long id) {
        return service.getArticleByID(id);
    }
    
    @GetMapping(value="/{description:[a-zA-Z]+}",produces=MediaType.APPLICATION_JSON_VALUE)
    public List<ArticleDto> getArticleByDescription(@PathVariable String description){
        return service.getArticleByDescription("%"+description+"%");
    }
    
  //  @ResponseStatus(code=HttpStatus.I_AM_A_TEAPOT)
    @DeleteMapping(value="/{id}",produces=MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> deleteArticle(@PathVariable long id) {
        try {
            service.DeleteArticle(id);
            return new ResponseEntity<>("L'article Id="+id+ " a été supprimé",HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("L'article Id="+id+ " n'existe pas",HttpStatus.NOT_FOUND);
        }
    }
    
    @PostMapping(value="",consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public ArticleDto addArticle(@RequestBody ArticleDto articleDto) {
        return service.saveOrUpdate(articleDto);
    }
    
    @PutMapping(value="/{id}",consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public ArticleDto updateArticle(@PathVariable long id,@RequestBody ArticleDto articleDto) {
        articleDto.setId(id);
        return service.saveOrUpdate(articleDto);
    }
    
    // Upload file
    @PostMapping(value="/image/{id}", consumes=MediaType.MULTIPART_FORM_DATA_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public ArticleDto addImage(@RequestParam("image") MultipartFile file,@PathVariable long id ) throws IOException {
        ArticleDto arDto=service.getArticleByID(id);
        System.out.println(file.getOriginalFilename() +  " " +file.getSize() + " " + file.getName());
        arDto.setImage(file.getBytes());
        return service.saveOrUpdate(arDto);
        
    }
    
    // Gestion des exceptions
    @GetMapping("/exception/io")
    public void genIOException() throws Exception {
        throw new IOException("Le Traitement de 'article a échoué");
    }
    
    @GetMapping("/exception/sql")
    public void genSqlException() throws Exception {
        throw new SQLException("Le Traitement de 'article a échoué");
    }
    
    @ExceptionHandler(IOException.class)
    public ResponseEntity<String> gestionIoException(IOException e){
        return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
    }
}



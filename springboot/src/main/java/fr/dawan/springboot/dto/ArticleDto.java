package fr.dawan.springboot.dto;

import java.time.LocalDate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonProperty;
@XmlRootElement(name="article")
@XmlAccessorType(XmlAccessType.FIELD)
public class ArticleDto {
    
    @XmlAttribute
    private long id;
    
    @XmlElement
    @JsonProperty("prixttc")
    private double prix;
    
    @XmlElement
    private String description;
    @XmlElement
    private LocalDate dateProduction;
    
    // pour ne pas serialiser
    @XmlTransient  // en xml
    //@JsonIgnore // en json
    private byte[] image;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDateProduction() {
        return dateProduction;
    }

    public void setDateProduction(LocalDate dateProduction) {
        this.dateProduction = dateProduction;
    }
    
    

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "ArticleDto [id=" + id + ", prix=" + prix + ", description=" + description + ", dateProduction="
                + dateProduction + "]";
    }

}

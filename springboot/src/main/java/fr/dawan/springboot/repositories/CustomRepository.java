package fr.dawan.springboot.repositories;

import java.util.List;

import fr.dawan.springboot.entities.Employe;

public interface CustomRepository {

    List<Employe> findBy(String prenom,String nom);
}

package fr.dawan.springboot.repositories;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import fr.dawan.springboot.entities.Article;
import fr.dawan.springboot.entities.Marque;

@Transactional
public interface ArticleRepository extends JpaRepository<Article, Long> {

    // sujet find
    List<Article> findByPrix(double prix);
    
 //   @Cacheable("prixless")
    List<Article> findByPrixLessThan(double prixMax);
    
    List<Article> findByPrixLessThanAndDateProductionBefore(double prixMax,LocalDate dateProdMax );
    
    List<Article> findByPrixBetween(double prixMin, double prixMax);
    
    List<Article> findByDescriptionLike(String model);
    
    List<Article> findByPrixGreaterThanOrderByPrixDescDescription(double prixMin);
    
    List<Article> findByMarque(Marque marque);
    // Expression de propriété
    List<Article> findByMarqueNomIgnoreCase(String nomMarque);
    
    // sujet exist
    boolean existsByDateProductionAfter(LocalDate date);
    
    // sujet count
    int countByPrixLessThan(double prix);
    
    // sujet delete => retour void ou int
    int deleteByMarque(Marque marque);
    
    // JPQL
    @Query("SELECT a FROM Article a WHERE a.prix<:montant")
    List<Article> findByPrixLessThanJPQL(@Param("montant") double prixMax);
    
    // SQL
    @Query(nativeQuery = true,value="SELECT * FROM articles WHERE prix<?1")
    List<Article> findByPrixLessThanSQL(double prixMax);
    
    // Expresion de chemin
    @Query("SELECT a FROM Article a WHERE a.marque.nom=:nomMarque")
    List<Article> findByMarqueJPQL(@Param("nomMarque") String nom);
    
    // Pagination
    Page<Article> findByPrixLessThan(double prixMax,Pageable page);
    
    // Limit => Top ou First
    Article findTopByOrderByPrixDesc();
    
    List<Article> findTop5ByOrderByPrix();
    
    // Procedure Stockée
    
    // Appel Explicite
    @Procedure("GET_COUNT_BY_PRIX2")
    int countINFMontant(double montant);
    
    // Appel Implicite
    @Procedure
    int get_count_by_prix2(@Param("montant")double montant);

    //SQL
    @Query(nativeQuery = true, value="GET_COUNT_BY_PRIX2(:prixMax")
    int countINFMontantSQL(@Param("prixMax") double montant);
}

package fr.dawan.springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.dawan.springboot.entities.Marque;

public interface MarqueRepository extends JpaRepository<Marque, Long> {
    @Query("SELECT m FROM Marque m JOIN m.articles a WHERE a.id=:id")
    Marque findJointure(@Param("id")long id);
}

package fr.dawan.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.dawan.springboot.services.ArticleService;
//@Component
//@Order(2)
public class ServiceRunner implements ApplicationRunner {

  @Autowired
  ArticleService service;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("Service Runner");
        System.out.println("---------------");
        
        service.getAllArticle(Pageable.unpaged()).forEach(System.out::println);
    }

}

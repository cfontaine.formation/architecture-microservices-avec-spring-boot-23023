package fr.dawan.springboot;

import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
//@EnableCaching
public class SpringbootApplication implements CommandLineRunner {

	public static void main(String[] args) {
		 SpringApplication.run(SpringbootApplication.class, args);
	    
		 // SpringApplication app=new SpringApplication(SpringbootApplication.class);
	    // app.setBannerMode(Mode.OFF);
	    // app.setAddCommandLineProperties(false);
	    // app.run(args);
	}

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Runner dans la classe springbootApplication");
        
    }
    
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}

package fr.dawan.springboot.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table
public class Authority implements GrantedAuthority {

    private static final long serialVersionUID = 1L;
    
    @Id
    private String authority;

    public Authority() {
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
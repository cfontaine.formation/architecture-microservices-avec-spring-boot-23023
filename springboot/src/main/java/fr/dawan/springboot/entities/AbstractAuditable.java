package fr.dawan.springboot.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractAuditable {

    @CreatedDate
    @Column(updatable = false)   // nullable=false
    private LocalDateTime created;
    
    @LastModifiedDate
    //@Column(nullable=false)   
    private LocalDateTime modified;
    
    @Column(updatable = false)   // nullable=false
    @CreatedBy
    private String createdby;
    
    @LastModifiedBy
    //@Column(nullable=false) 
    private String modifiedby;

}

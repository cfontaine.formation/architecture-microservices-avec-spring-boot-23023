package fr.dawan.springboot.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import fr.dawan.springboot.enums.Contrat;


@Entity
@Table(name="employes")
//@TableGenerator(name="employeGen")
//@SequenceGenerator(name="employeSeq")
public class Employe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // @GeneratedValue(strategy = GenerationType.TABLE,generator = "employeGen")
   //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employeSeq")
    private long id;
    
    @Version
    private int version;
    
    @Column(length = 60)
    private String prenom;
    
    @Column(length = 60,nullable = false)
    private String nom;
    
    @Column(name="date_naissance",nullable = false)
    private LocalDate dateNaissance;
    
    @Transient
    private int nePasPersister;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private Contrat contrat;

    @Embedded
    private Adresse adressePro;
    
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "rue", column = @Column(name="rue_perso")),
            @AttributeOverride(name = "ville", column = @Column(name="ville_perso")),
            @AttributeOverride(name = "codePostal", column = @Column(name="code_postal_perso"))
    })
    private Adresse adressePerso;
    
    @OneToOne
    private PlaceParking placeParking;
    
    public Employe() {

    }

    public Employe(String prenom, String nom, LocalDate dateNaissance) {
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Contrat getContrat() {
        return contrat;
    }

    public void setContrat(Contrat contrat) {
        this.contrat = contrat;
    }

    public Adresse getAdressePro() {
        return adressePro;
    }

    public void setAdressePro(Adresse adressePro) {
        this.adressePro = adressePro;
    }

    public Adresse getAdressePerso() {
        return adressePerso;
    }

    public void setAdressePerso(Adresse adressePerso) {
        this.adressePerso = adressePerso;
    }

    public PlaceParking getPlaceParking() {
        return placeParking;
    }

    public void setPlaceParking(PlaceParking placeParking) {
        this.placeParking = placeParking;
    }

    @Override
    public String toString() {
        return "Employe [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", dateNaissance=" + dateNaissance + "]";
    }
 
}

package fr.dawan.springboot.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;
@Entity
public class Commande implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    private LocalDateTime heureCommande;
    
    @OneToMany(mappedBy = "commande")
    private List<CommandePizza> pizzas=new ArrayList<>();

    public Commande() {

    }

    public Commande(LocalDateTime heureCommande) {
        this.heureCommande = heureCommande;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public LocalDateTime getHeureCommande() {
        return heureCommande;
    }

    public void setHeureCommande(LocalDateTime heureCommande) {
        this.heureCommande = heureCommande;
    }
    
    
}

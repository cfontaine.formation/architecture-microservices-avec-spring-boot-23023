package fr.dawan.springboot.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;
@Entity
@Table(name="fournisseurs")
public class Fournisseur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    private String nom;
    
    @ManyToMany
//    @JoinTable(name="fournisseur2Article",
//    joinColumns = @JoinColumn(name="id_article",referencedColumnName = "id"),
//    inverseJoinColumns = @JoinColumn(name="id_fournisseur",referencedColumnName = "id")
//            )
    private List<Article> articles=new ArrayList<>();

    public Fournisseur() {

    }

    public Fournisseur(String nom) {
        super();
        this.nom = nom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "Fournisseur [id=" + id + ", version=" + version + ", nom=" + nom + "]";
    }

}

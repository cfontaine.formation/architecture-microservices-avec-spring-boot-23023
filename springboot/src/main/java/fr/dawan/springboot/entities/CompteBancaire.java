package fr.dawan.springboot.entities;

import java.io.Serializable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
@Entity

// 1 single table
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//@DiscriminatorColumn(name="type_compte")
//@DiscriminatorValue("CB")

//2 une table par classe
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

// 3 Jointure
@Inheritance(strategy = InheritanceType.JOINED)
public class CompteBancaire implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    private long id; 
    
    private String titulaire;
    
    private double solde;

    public CompteBancaire() {

    }

    public CompteBancaire(String titulaire, double solde) {
        this.titulaire = titulaire;
        this.solde = solde;
    }



    public String getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    
}

package fr.dawan.springboot.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class CommandePizzaId implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private long commandeId;
    
    private long pizzaId;

    public CommandePizzaId() {

    }

    public CommandePizzaId(long commandeId, long pizzaId) {
        super();
        this.commandeId = commandeId;
        this.pizzaId = pizzaId;
    }

    public long getCommandeId() {
        return commandeId;
    }

    public void setCommandeId(long commandeId) {
        this.commandeId = commandeId;
    }

    public long getPizzaId() {
        return pizzaId;
    }

    public void setPizzaId(long pizzaId) {
        this.pizzaId = pizzaId;
    }
    
    

}

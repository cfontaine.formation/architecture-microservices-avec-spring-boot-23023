package fr.dawan.springboot.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="articles")
public class Article extends AbstractAuditable implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    private String description;
    
    private double prix;
    
    private LocalDate dateProduction;
    
    @Lob
    private byte[] image;
    
    @ManyToOne(cascade = CascadeType.PERSIST)
    //@JoinColumn(name = "id_marque")
    private Marque marque;
    
    @ManyToMany(mappedBy = "articles")
    private List<Fournisseur> fournisseurs=new ArrayList<>();

    public Article() {

    }

    public Article(String description, double prix, LocalDate dateProduction) {

        this.description = description;
        this.prix = prix;
        this.dateProduction = dateProduction;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public LocalDate getDateProduction() {
        return dateProduction;
    }

    public void setDateProduction(LocalDate dateProduction) {
        this.dateProduction = dateProduction;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public List<Fournisseur> getFournisseurs() {
        return fournisseurs;
    }

    public void setFournisseurs(List<Fournisseur> fournisseurs) {
        this.fournisseurs = fournisseurs;
    }

    @Override
    public String toString() {
        return "Article [id=" + id + ", version=" + version + ", description=" + description + ", prix=" + prix
                + ", dateProduction=" + dateProduction + "]";
    }
    
}

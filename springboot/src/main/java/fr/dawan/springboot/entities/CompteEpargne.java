package fr.dawan.springboot.entities;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
//@DiscriminatorValue("CE")
public class CompteEpargne extends CompteBancaire {

    private static final long serialVersionUID = 1L;
    
    private double taux=3.0;

    public CompteEpargne() {
        super();
    }

    public CompteEpargne(String titulaire, double solde,double taux) {
        super(titulaire, solde);
        this.taux=taux;
    }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }

    
    
}

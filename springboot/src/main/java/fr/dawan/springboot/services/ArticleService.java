package fr.dawan.springboot.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.springboot.dto.ArticleDto;

public interface ArticleService {

    List<ArticleDto> getAllArticle(Pageable page);
    
    ArticleDto getArticleByID(long id);
    
    List<ArticleDto> getArticleByDescription(String modelDescription);
    
    ArticleDto saveOrUpdate(ArticleDto articleDto);
    
    void DeleteArticle(long id);
}

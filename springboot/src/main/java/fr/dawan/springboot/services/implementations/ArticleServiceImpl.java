package fr.dawan.springboot.services.implementations;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.dto.ArticleDto;
import fr.dawan.springboot.entities.Article;
import fr.dawan.springboot.repositories.ArticleRepository;
import fr.dawan.springboot.services.ArticleService;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ModelMapper mapper;

    @Transactional(readOnly = true)
    @Override
    public List<ArticleDto> getAllArticle(Pageable page) {
//        List<Article> lst=articleRepository.findAll(page).getContent();
//        List<ArticleDto> lstDto=new ArrayList<>();
//        for(Article a : lst) {
//            lstDto.add(mapper.map(a, ArticleDto.class));
//        }
//        return lstDto;
       return articleRepository.findAll(page).get().map(a -> mapper.map(a,ArticleDto.class)).collect(Collectors.toList());
    }

    @Override
    public ArticleDto getArticleByID(long id) {
        Article a = articleRepository.findById(id).get();
        return mapper.map(a, ArticleDto.class);
    }

    @Override
    public List<ArticleDto> getArticleByDescription(String modelDescription) {
//        List<Article> lst=articleRepository.findByDescriptionLike(modelDescription);
//        List<ArticleDto> lstDto=new ArrayList<>();
//        for(Article a : lst) {
//            lstDto.add(mapper.map(a, ArticleDto.class));
//        }
//        return lstDto;
        return articleRepository.findByDescriptionLike(modelDescription).stream().map(a -> mapper.map(a,ArticleDto.class)).collect(Collectors.toList());
    }

    @Override
    public ArticleDto saveOrUpdate(ArticleDto articleDto) {
        Article tmp = articleRepository.saveAndFlush(mapper.map(articleDto, Article.class));
        return mapper.map(tmp, ArticleDto.class);
    }

    @Override
    public void DeleteArticle(long id) {
        articleRepository.deleteById(id);
    }

}
